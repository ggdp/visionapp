package br.ufsm.csi.VisionApp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VisionAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(VisionAppApplication.class, args);
	}

}
